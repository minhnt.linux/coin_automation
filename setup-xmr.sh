#! /bin/bash
sudo apt-get update
sudo apt-get -y install git build-essential cmake libuv1-dev
git clone https://github.com/xmrig/xmrig.git
cd xmrig
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DUV_LIBRARY=/usr/lib/x86_64-linux-gnu/libuv.a
make
cd .. && cd .. && ./start-xmr.sh